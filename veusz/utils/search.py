#!/usr/bin/python
# -*- coding: utf-8 -*-
def iter_widgets(base, typename, direction=0):
    """Yields all widgets of type `typename` starting from `base` widget.
    The search can be restricted to upward (`direction`=-1), downward (`direction`=1) or both (`direction`=0)."""
    if isinstance(typename, str):
        typename = [typename]
    if not base:
        yield None
        return
    if base.typename in typename:
        yield base
    next_direction = None
    if direction==0:
        next_direction=-1
    elif hasattr(direction,'__iter__'):
        direction, next_direction = direction
    # Search down in the object tree
    if direction >= 0:
        for obj in base.children:
            if obj.typename in typename:
                yield obj
        for obj in base.children:
            for found in iter_widgets(obj, typename, direction=1):
                if found:
                    yield found
    elif direction < 0:
        for found in iter_widgets(base.parent, typename, direction=-1):
            yield found
    if next_direction is not None:
        for found in iter_widgets(base.parent, typename, direction=next_direction):
            yield found
    yield None


def searchFirstOccurrence(base, typename, direction=(1,-1)):
    """Search for the nearest occurrence of a widget of type `typename` starting from `base`.
    The search can be restricted to upward (`direction`=-1), downward (`direction`=1),
    both (`direction`=0) or sequence of directions (-1,1)"""
    for wg in iter_widgets(base, typename, direction):
        if wg:
            return wg