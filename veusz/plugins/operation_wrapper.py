#!/usr/bin/python
# -*- coding: utf-8 -*-
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)


class OperationWrapper(object):
    """Helper class for operation-based objects like ToolPlugins or custom widgets"""
    name = 'OperationWrapper'
    _ops = False
    preserve = None
    _doc = None

    @property
    def ops(self):
        if not self._ops:
            self._ops = []
        return self._ops

    @ops.setter
    def ops(self, val):
        self._ops = val
        
    @property
    def doc(self):
        if self._doc:
            return self._doc
        return self.document

    @doc.setter 
    def doc(self, doc):
        self._doc = doc

    def apply_ops(self, descr=False):
        from .. import document
        if not descr:
            if getattr(self, 'name', False):
                descr = self.name
            else:
                descr = self.typename
        if len(self.ops) > 0:
            self.doc.applyOperation(
                document.OperationMultiple(self.ops, descr=descr))
        self.ops = []

    def toset(self, out, name, val):
        """Set `name` to `val` on `out` widget"""
        from .. import document
        stn = self.doc.resolveSettingPath(out, name)
        old = stn.get()
        if old != val:
            self.ops.append(document.OperationSettingSet(stn, val))
            return False
        return True

    def cpset(self, ref, out, name):
        """Copy setting `name` from `ref` to `out`"""
        val = self.doc.resolveSettingPath(ref, name).get()
        return self.toset(out, name, val)

    def eqset(self, ref, name):
        """Set DataPoint setting `name` equal to the same setting value on `ref` widget."""
        return self.cpset(ref, self, name)

    def dict_toset(self, out, props, preserve=None):
        if preserve is None:
            preserve = self.preserve
        pd = {}
        if preserve:
            pd = getattr(out, 'm_auto', {})
#         print 'found preserve dict',pd
        for k, v in pd.items():
            if k not in props:
                continue
            cur = self.doc.resolveSettingPath(out, k).get()
            # It took a different value than the auto-arranged one
            if cur != v:
                # Remove from auto assign
                props.pop(k)
        for name, value in props.items():
            self.toset(out, name, value)
        # Update the m_auto attribute
        if len(pd) > 0:
            out.m_auto.update(props)
        # Create it if was missing
        elif preserve:
            out.m_auto = props
        return True

